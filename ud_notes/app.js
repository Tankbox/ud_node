const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');
const notes = require('./notes.js');

const titleOptions = {
  describe: 'Title of note',
  demand: true,
  alias: 't'
};
const bodyOptions = {
  describe: 'Body of note',
  demand: true,
  alias: 'b'
};

const argv = yargs
  .command('add', 'Add a new note', {
      title: titleOptions,
      body: bodyOptions
  })
  .command('list', 'List all notes')
  .command('read', 'Read a note', {
    title: titleOptions
  })
  .command('remove', 'Remove a note', {
    title: titleOptions
  })
  .help()
  .argv;

var command = argv._[0];

if (command === 'add') {
  var note = notes.addNote(argv.title, argv.body);
  console.log('--------------------------');
  if (typeof note !== 'undefined') {
    console.log('Note successfully created');
    notes.logNote(note);
  } else {
    console.log('This note already exists and was note saved');
  }
  console.log('--------------------------');
} else if (command === 'list') {
  var allNotes = notes.getAll();
  allNotes.forEach((note) => notes.logNote(note));
} else if (command === 'read') {
  var note = notes.getNote(argv.title);
  console.log('--------------------------');
  if (typeof note !== 'undefined') {
    console.log('Note successfully read');
    notes.logNote(note);
  } else {
    console.log('Note not found');
  }
  console.log('--------------------------');
} else if (command === 'remove') {
  var noteRemoved = notes.removeNote(argv.title);
  var message = (noteRemoved) ? 'Note was removed' : 'Note not found';
  console.log('--------------------------');
  console.log(message);
  console.log('--------------------------');
} else {
  console.log('Command not recognized');
}
