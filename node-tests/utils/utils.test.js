const expect = require('expect');
const utils = require('./utils');

describe('Utils', () => {
  describe('#add', () => {
    it('should add two numbers', () => {
      var result = utils.add(33, 11);
      expect(result).toBeA('number').toBe(44);
    });
    it('should add two numbers asynchronously', (done) => {
      utils.asyncAdd(4, 3, (sum) => {
        expect(sum)
          .toBeA('number')
          .toBe(7);
        done();
      });
    });
  });

  describe('#square', () => {
    it('should square a number', () => {
      var result = utils.square(3);

      expect(result).toBeA('number').toBe(9);
    });
    it('should square a number asynchronously', (done) => {
      utils.asyncSquare(4, (result) => {
        expect(result)
          .toBeA('number')
          .toBe(16);
        done();
      });
    });
  });
});

describe('Misc', () => {
  it('should set first and last name for a user', () => {
    var user = {
      age: 26,
      location: 'Austin'
    }

    user = utils.setName(user, 'Joe Cowen-Richards');

    expect(user)
      .toBeA('object')
      .toIncludeKey('firstName')
      .toIncludeKey('lastName');
    expect(user.firstName)
      .toBeA('string')
      .toBe('Joe');
    expect(user.lastName)
      .toBeA('string')
      .toBe('Cowen-Richards');
  });
});
