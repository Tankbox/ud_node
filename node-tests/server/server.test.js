const request = require('supertest');
const expect = require('expect');
const app = require('./server').app;

describe('Server', () => {
  describe('GET /', () => {
    it('should return hello world response', (done) => {
      request(app)
        .get('/')
        .expect(404)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect((response) => {
          expect(response.body)
            .toIncludeKey('error');
          expect(response.body.error)
            .toBe('Page not found.');
        })
        .end(done);
    });
  });

  describe('GET /users', () => {
    it('should return my user information', (done) => {
      request(app)
        .get('/users')
        .expect(200)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect((response) => {
          expect(response.body)
            .toBeAn('array')
            .toInclude({
              name: 'Joe',
              age: 26
            });
        })
        .end(done);
    });
  });
});
