const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs
  .options({
    address: {
      demand: true,
      alias: 'a',
      describe: 'Address to fetch weather data',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

geocode.geocodeAddress(argv.address, (errorMessage, results) => {
  if (errorMessage) {
    console.log(errorMessage);
  } else {
    console.log(results.address);
    var coordinates = {
      lat: results.latitude,
      lng: results.longitude
    };
    weather.getWeather(coordinates, (errorMessage, weatherResults) => {
      if (errorMessage) {
        console.log(errorMessage);
      } else {
        console.log(`Currently it's ${weatherResults.body.currently.temperature}, but if feels like ${weatherResults.body.currently.apparentTemperature}`)
      }
    });
  }
});
