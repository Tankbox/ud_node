const request = require('request');

var getWeather = (coordinates, callback) => {
  request({
    url: `https://api.darksky.net/forecast/7abee8ae04a6388d271ba6ce2e469432/${coordinates.lat},${coordinates.lng}`,
    json: true
  }, (error, response, body) => {
    if (error) {
      callback('Unable to connect to Dark Sky servers.');
    } else if (response.statusMessage === 'OK') {
      callback(undefined, response);
    } else {
      callback('Unable to find weather for those coordinates.');
    }
  });
};

module.exports = {
  getWeather
};
